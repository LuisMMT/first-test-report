package com.ni.reports.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "reports")
data class Report(
    @PrimaryKey
    val uuid: Int?,
    val description: String?,
    val category: String?,
    val operation: String?,
    val amount: Double?,
    val status: String?,
    val creation_date: String?)