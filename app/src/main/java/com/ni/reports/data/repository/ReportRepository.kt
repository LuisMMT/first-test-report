package com.ni.reports.data.repository

import com.ni.punkapi.data.remote.ReportRemoteDataSource
import com.ni.reports.data.local.ReportDao
import com.ni.reports.utils.performGetOperation
import com.ni.reports.utils.performGetOperationLocal
import javax.inject.Inject

class ReportRepository @Inject constructor(
    private val remoteDataSource: ReportRemoteDataSource,
    private val localDataSource: ReportDao
){

    fun getReports() = performGetOperation(
        databaseQuery = { localDataSource.getAllReports() },
        networkCall = { remoteDataSource.getReports()},
        saveCallResult = { localDataSource.insertAll(it) }
    )
}
