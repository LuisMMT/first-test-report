package com.ni.punkapi.data.remote

import javax.inject.Inject

class ReportRemoteDataSource @Inject constructor(
    private val reportService: ReportService
):BaseDataSource(){
    suspend fun getReports() = getResult{ reportService.getAllReports()}
    suspend fun getReport(id:Int) = getResult{reportService.getReport(id)}

}