package com.ni.reports.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.ni.reports.data.entities.Report

@Database(entities = [Report::class],version = 1,exportSchema = false)
abstract class AppDatabase : RoomDatabase(){
    abstract  fun reportDao(): ReportDao

    companion object{
        @Volatile private var instance: AppDatabase? =null

        fun getDatabase(context: Context):AppDatabase= instance?: synchronized(this){ instance?:buildDatabase(context).also{ instance = it}}

        private fun buildDatabase(appContext: Context)= Room.databaseBuilder(appContext,AppDatabase::class.java,"reports").fallbackToDestructiveMigration().build()
    }

}