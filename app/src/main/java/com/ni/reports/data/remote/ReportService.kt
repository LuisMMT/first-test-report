package com.ni.punkapi.data.remote

import com.ni.reports.data.entities.Report
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface ReportService {
    @GET("06ec83050ec79170b10a11d1d4924dfe/raw/ad791cddcff6df2ec424bfa3da7cdb86f266c57e/transactions.json")
    suspend fun getAllReports() : Response<List<Report>>

    @GET("character/{id}")
    suspend fun getReport(@Path("id") id: Int): Response<Report>
}