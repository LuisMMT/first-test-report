package com.ni.reports.data.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ni.reports.data.entities.Report

@Dao
interface ReportDao {
    @Query("SELECT * FROM reports")
    fun getAllReports(): LiveData<List<Report>>

    @Query("SELECT * FROM reports WHERE uuid = :id")
    fun getReport(id:Int): LiveData<Report>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(beer: List<Report>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(beer: Report)
}