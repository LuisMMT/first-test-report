package com.ni.punkapi.di

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.ni.punkapi.data.remote.ReportRemoteDataSource
import com.ni.punkapi.data.remote.ReportService
import com.ni.reports.data.local.AppDatabase
import com.ni.reports.data.local.ReportDao
import com.ni.reports.data.repository.ReportRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.security.KeyManagementException
import java.security.NoSuchAlgorithmException
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object AppModule {

    private var mClient: OkHttpClient? = null

    val client: OkHttpClient
        @Throws(NoSuchAlgorithmException::class, KeyManagementException::class)
        get() {
            if (mClient == null) {
                val interceptor = HttpLoggingInterceptor()
                interceptor.level = HttpLoggingInterceptor.Level.BODY


                val httpBuilder = OkHttpClient.Builder()
                httpBuilder
                        .connectTimeout(15, TimeUnit.SECONDS)
                        .readTimeout(20, TimeUnit.SECONDS)
                        .addInterceptor(interceptor)  /// show all JSON in logCat
                mClient = httpBuilder.build()

            }
            return mClient!!
        }

    @Singleton
    @Provides
    fun provideRetrofit(gson: Gson) : Retrofit = Retrofit.Builder()
        .baseUrl("https://gist.githubusercontent.com/astrocumbia/")
        .addConverterFactory(GsonConverterFactory.create(gson))
        .client(client)
        .build()

    @Provides
    fun provideGson(): Gson = GsonBuilder().create()

    @Provides
    fun provideBeerService(retrofit: Retrofit): ReportService = retrofit.create(ReportService::class.java)

    @Singleton
    @Provides
    fun provideBeerRemoteDataSource(ReportService: ReportService) = ReportRemoteDataSource(ReportService)

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext appContext: Context) = AppDatabase.getDatabase(appContext)

    @Singleton
    @Provides
    fun provideBeerDao(db: AppDatabase) = db.reportDao()

    @Singleton
    @Provides
    fun provideRepository(remoteDataSource: ReportRemoteDataSource,
                          localDataSource: ReportDao
    ) =
        ReportRepository(remoteDataSource, localDataSource)

}