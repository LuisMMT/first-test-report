package com.ni.reports.ui

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import com.ni.reports.data.repository.ReportRepository

class ReportViewModel @ViewModelInject constructor(
    private val repository: ReportRepository
): ViewModel(){
    val beers = repository.getReports()
}