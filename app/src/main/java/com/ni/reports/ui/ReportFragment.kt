package com.ni.reports.ui
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater

import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels


import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.ni.reports.R
import com.ni.reports.data.entities.Report
import com.ni.reports.databinding.FragmentReportBinding

import com.ni.reports.utils.Resource
import com.ni.reports.utils.autoCleared
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_report.*
import java.math.RoundingMode
import java.time.Month
import java.util.*

@AndroidEntryPoint
class ReportFragment : Fragment() {
    private var binding: FragmentReportBinding by autoCleared()
    private val viewModel: ReportViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding =  FragmentReportBinding.inflate(inflater,container,false)
        return  binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        view.findViewById<Button>(R.id.button_first).setOnClickListener {
            findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment)
        }
        viewModel.beers
        setUpObservers()
    }
    private fun setUpObservers() {
        viewModel.beers.observe(viewLifecycleOwner, Observer {
            when(it.status){
                Resource.Status.SUCCESS->{
                    binReport(it.data!!)
                    binding.progressBar.visibility = View.GONE
                    binding.reportCl.visibility = View.VISIBLE
                }
                Resource.Status.ERROR-> Toast.makeText(activity,it.message, Toast.LENGTH_LONG).show()

                Resource.Status.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                    binding.reportCl.visibility = View.GONE
                }
            }
        })
    }

    private fun binReport(data: List<Report>) {

        var pendingJan=0
        var rejectJan=0
        var incomeJan=0.0
        var outJan =0.0
        var totalCategoryJan =0.0
        var totalTransferJn=0.0
        var totalServiceJn=0.0
        var totalFunJn=0.0

        var pendingFeb=0
        var rejectFeb=0
        var incomeFeb=0.0
        var outFeb =0.0
        var totalCategoryFeb =0.0
        var totalTransferFeb=0.0
        var totalServiceFeb=0.0
        var totalFunFeb=0.0

        var pendingMarz=0
        var rejectMarz=0
        var incomeMarz=0.0
        var outMarz =0.0
        var totalCategoryMarz =0.0
        var totalTransferMarz=0.0
        var totalServiceMarz=0.0
        var totalFunMarz=0.0

        var pendingApr=0
        var rejectApr=0
        var incomeApr=0.0
        var outApr =0.0
        var totalCategoryApr =0.0
        var totalTransferApr=0.0
        var totalServiceApr=0.0
        var totalFunApr=0.0

        var pendingMay=0
        var rejectMay=0
        var incomeMay=0.0
        var outMay=0.0
        var totalCategoryMay=0.0
        var totalTransMay=0.0
        var totalServiceMay=0.0
        var totalFunMay=0.0

        var pendingJun=0
        var rejectJun=0
        var incomeJun=0.0
        var outJun =0.0
        var totalCategoryJun=0.0
        var totalTransJun=0.0
        var totalServiceJun=0.0
        var totalFunJun=0.0

        var pendingJul=0
        var rejecJul=0
        var incomeJul=0.0
        var outJul =0.0
        var totalCategoryJul=0.0
        var totalTransJul=0.0
        var totalServiceJul=0.0
        var totalFunJul=0.0

        var pendingAug=0
        var rejectAug=0
        var incomeAug=0.0
        var outAug =0.0
        var totalCategoryAug=0.0
        var totalTransAug=0.0
        var totalServiceAug=0.0
        var totalFunAug=0.0

        var pendingSep=0
        var rejectSep=0
        var incomeSep=0.0
        var outSep=0.0
        var totalCategorySep=0.0
        var totalTransSep=0.0
        var totalServiceSep=0.0
        var totalFunSep=0.0

        var pendingOct=0
        var rejectOct=0
        var incomeOct=0.0
        var outOct =0.0
        var totalCategoryOct=0.0
        var totalTransOct=0.0
        var totalServiceOct=0.0
        var totalFunOct=0.0

        var pendingNov=0
        var rejectNov=0
        var incomeNov=0.0
        var outNov=0.0
        var totalCategoryNov=0.0
        var totalTransNov=0.0
        var totalServiceNov=0.0
        var totalFunNov=0.0

        var pendingDec=0
        var rejectDec=0
        var incomeDec=0.0
        var outDec =0.0
        var totalCategoryDec=0.0
        var totalTransDec=0.0
        var totalServiceDec=0.0
        var totalFunDec=0.0

        for(i in data){

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                if(((Date(i.creation_date).month)+1).equals(Month.JANUARY.value)){
                    totalCategoryJan+=1
                    if(i.status.equals("pending")) {
                        pendingJan += 1
                    }
                    if(i.status.equals("rejected")) {
                        rejectJan +=1
                    }
                    if (i.operation.equals("in")) {
                        incomeJan = incomeJan+ i.amount!!
                    }
                    if (i.operation.equals("out")) {
                        outJan = outJan +i.amount!!
                    }
                    if (i.category.equals("Alimentacion")){
                        totalTransferJn+=1
                    }
                    if (i.category.equals("Servicios")){
                        totalServiceJn+=1
                    }
                    if (i.category.equals("Entretenimiento")){
                        totalFunJn+=1
                    }
                }
                if(((Date(i.creation_date).month)+1).equals(Month.FEBRUARY.value)){
                    totalCategoryFeb+=1
                    if(i.status.equals("pending")) {
                        pendingFeb += 1
                    }
                    if(i.status.equals("rejected")) {
                        rejectFeb +=1
                    }
                    if (i.operation.equals("in")) {
                        incomeFeb += i.amount!!
                    }
                    if (i.operation.equals("out")) {
                        outFeb += i.amount!!
                    }
                    if (i.category.equals("Alimentacion")){
                        totalTransferFeb+=1
                    }
                    if (i.category.equals("Servicios")){
                        totalServiceFeb+=1
                    }
                    if (i.category.equals("Entretenimiento")){
                        totalFunFeb+=1
                    }
                }
                if(((Date(i.creation_date).month)+1).equals(Month.MARCH.value)){
                    totalCategoryMarz+=1
                    if(i.status.equals("pending")) {
                        pendingMarz += 1
                    }
                    if(i.status.equals("rejected")) {
                        rejectMarz +=1
                    }
                    if (i.operation.equals("in")) {
                        incomeMarz += i.amount!!
                    }
                    if (i.operation.equals("out")) {
                        outMarz += i.amount!!
                    }
                    if (i.category.equals("Alimentacion")){
                        totalTransferMarz+=1
                    }
                    if (i.category.equals("Servicios")){
                        totalServiceMarz+=1
                    }
                    if (i.category.equals("Entretenimiento")){
                        totalFunMarz+=1
                    }
                }
                if(((Date(i.creation_date).month)+1).equals(Month.APRIL.value)){
                    totalCategoryApr+=1
                    if(i.status.equals("pending")) {
                        pendingApr += 1
                    }
                    if(i.status.equals("rejected")) {
                        rejectApr +=1
                    }
                    if (i.operation.equals("in")) {
                        incomeApr = incomeApr+ i.amount!!
                    }
                    if (i.operation.equals("out")) {
                        outApr = outApr +i.amount!!
                    }
                    if (i.category.equals("Alimentacion")){
                        totalTransferApr+=1
                    }
                    if (i.category.equals("Servicios")){
                        totalServiceApr+=1
                    }
                    if (i.category.equals("Entretenimiento")){
                        totalFunApr+=1
                    }
                }
                if(((Date(i.creation_date).month)+1).equals(Month.MAY.value)){
                    totalCategoryMay+=1
                    if(i.status.equals("pending")) {
                        pendingMay += 1
                    }
                    if(i.status.equals("rejected")) {
                        rejectMay +=1
                    }
                    if (i.operation.equals("in")) {
                        incomeMay = incomeMay+ i.amount!!
                    }
                    if (i.operation.equals("out")) {
                        outMay = outMay +i.amount!!
                    }
                    if (i.category.equals("Alimentacion")){
                        totalTransMay +=1
                    }
                    if (i.category.equals("Servicios")){
                        totalServiceMay+=1
                    }
                    if (i.category.equals("Entretenimiento")){
                        totalFunMay+=1
                    }
                }
                if(((Date(i.creation_date).month)+1).equals(Month.JUNE.value)){
                    totalCategoryJun+=1
                    if(i.status.equals("pending")) {
                        pendingJun += 1
                    }
                    if(i.status.equals("rejected")) {
                        rejectJun +=1
                    }
                    if (i.operation.equals("in")) {
                        incomeJun = incomeJun+ i.amount!!
                    }
                    if (i.operation.equals("out")) {
                        outJun = outJun +i.amount!!
                    }
                    if (i.category.equals("Alimentacion")){
                        totalTransJun+=1
                    }
                    if (i.category.equals("Servicios")){
                        totalServiceJun+=1
                    }
                    if (i.category.equals("Entretenimiento")){
                        totalFunJun+=1
                    }
                }
                if(((Date(i.creation_date).month)+1).equals(Month.JULY.value)){
                    totalCategoryJul+=1
                    if(i.status.equals("pending")) {
                        pendingJul += 1
                    }
                    if(i.status.equals("rejected")) {
                        rejecJul +=1
                    }
                    if (i.operation.equals("in")) {
                        incomeJul = incomeJul+ i.amount!!
                    }
                    if (i.operation.equals("out")) {
                        outJul = outJul +i.amount!!
                    }
                    if (i.category.equals("Alimentacion")){
                        totalTransJul+=1
                    }
                    if (i.category.equals("Servicios")){
                        totalServiceJul+=1
                    }
                    if (i.category.equals("Entretenimiento")){
                        totalFunJul+=1
                    }
                }
                if(((Date(i.creation_date).month)+1).equals(Month.AUGUST.value)){
                    totalCategoryAug+=1
                    if(i.status.equals("pending")) {
                        pendingAug += 1
                    }
                    if(i.status.equals("rejected")) {
                        rejectAug +=1
                    }
                    if (i.operation.equals("in")) {
                        incomeAug = incomeAug+ i.amount!!
                    }
                    if (i.operation.equals("out")) {
                        outAug = outAug +i.amount!!
                    }
                    if (i.category.equals("Alimentacion")){
                        totalTransAug+=1
                    }
                    if (i.category.equals("Servicios")){
                        totalServiceAug+=1
                    }
                    if (i.category.equals("Entretenimiento")){
                        totalFunAug+=1
                    }
                }
                if(((Date(i.creation_date).month)+1).equals(Month.SEPTEMBER.value)){
                    totalCategorySep+=1
                    if(i.status.equals("pending")) {
                        pendingSep += 1
                    }
                    if(i.status.equals("rejected")) {
                        rejectSep +=1
                    }
                    if (i.operation.equals("in")) {
                        incomeSep = incomeSep+ i.amount!!
                    }
                    if (i.operation.equals("out")) {
                        outSep = outSep +i.amount!!
                    }
                    if (i.category.equals("Alimentacion")){
                        totalTransSep+=1
                    }
                    if (i.category.equals("Servicios")){
                        totalServiceSep+=1
                    }
                    if (i.category.equals("Entretenimiento")){
                        totalFunSep+=1
                    }
                }
                if(((Date(i.creation_date).month)+1).equals(Month.OCTOBER.value)){
                    totalCategoryOct+=1
                    if(i.status.equals("pending")) {
                        pendingOct += 1
                    }
                    if(i.status.equals("rejected")) {
                        rejectOct +=1
                    }
                    if (i.operation.equals("in")) {
                        incomeOct = incomeOct+ i.amount!!
                    }
                    if (i.operation.equals("out")) {
                        outOct = outOct +i.amount!!
                    }
                    if (i.category.equals("Alimentacion")){
                        totalTransOct+=1
                    }
                    if (i.category.equals("Servicios")){
                        totalServiceOct+=1
                    }
                    if (i.category.equals("Entretenimiento")){
                        totalFunOct+=1
                    }
                }
                if(((Date(i.creation_date).month)+1).equals(Month.NOVEMBER.value)){
                    totalCategoryNov+=1
                    if(i.status.equals("pending")) {
                        pendingNov += 1
                    }
                    if(i.status.equals("rejected")) {
                        rejectNov +=1
                    }
                    if (i.operation.equals("in")) {
                        incomeNov = incomeNov+ i.amount!!
                    }
                    if (i.operation.equals("out")) {
                        outNov = outNov +i.amount!!
                    }
                    if (i.category.equals("Alimentacion")){
                        totalTransNov+=1
                    }
                    if (i.category.equals("Servicios")){
                        totalServiceNov+=1
                    }
                    if (i.category.equals("Entretenimiento")){
                        totalFunNov+=1
                    }
                }
                if(((Date(i.creation_date).month)+1).equals(Month.DECEMBER.value)){
                    totalCategoryDec+=1
                    if(i.status.equals("pending")) {
                        pendingDec += 1
                    }
                    if(i.status.equals("rejected")) {
                        rejectDec +=1
                    }
                    if (i.operation.equals("in")) {
                        incomeDec = incomeDec+ i.amount!!
                    }
                    if (i.operation.equals("out")) {
                        outDec = outDec +i.amount!!
                    }
                    if (i.category.equals("Alimentacion")){
                        totalTransDec+=1
                    }
                    if (i.category.equals("Servicios")){
                        totalServiceDec+=1
                    }
                    if (i.category.equals("Entretenimiento")){
                        totalFunDec+=1
                    }
                }
            }
        }
         binding.monthjanuary.text = getString(R.string.january)
        binding.pendingjan.text = pendingJan.toString() +" " +getString(R.string.pending)
        binding.rejectedjan.text = rejectJan.toString()+" " + getString(R.string.rejected)
        binding.incomejan.text = incomeJan.toBigDecimal().setScale(1, RoundingMode.UP).toDouble().toString() +" "+ getString(R.string.income)
        binding.outjan.text = outJan.toBigDecimal().setScale(1, RoundingMode.UP).toDouble().toString()+" "+ getString(R.string.expenses)
        binding.transjan.text = getString(R.string.transfer) +" "+ ((totalTransferJn/totalCategoryJan)*100).toBigDecimal().setScale(1, RoundingMode.UP).toDouble()
        binding.servicejan.text = getString(R.string.service)+" " + ((totalServiceJn/totalCategoryJan)*100).toBigDecimal().setScale(1, RoundingMode.UP).toDouble()
        binding.funjan.text = getString(R.string.funny)+" " + ((totalFunJn/totalCategoryJan)*100).toBigDecimal().setScale(1, RoundingMode.UP).toDouble()


        binding.monthFeb.text = getString(R.string.february)
        binding.pendingfeb.text = pendingFeb.toString()+" " + getString(R.string.pending)
        binding.rejectedfeb.text = rejectFeb.toString()+" " +getString(R.string.rejected)
        binding.incomefeb.text = "$ "+incomeFeb.toBigDecimal().setScale(1, RoundingMode.UP).toDouble().toString() +" "+ getString(R.string.income)
        binding.outfeb.text = "$" + outFeb.toBigDecimal().setScale(1, RoundingMode.UP).toDouble().toString() +" "+ getString(R.string.expenses)
        binding.transfeb.text = getString(R.string.transfer)+" " + ((totalTransferFeb/totalCategoryFeb)*100).toBigDecimal().setScale(1, RoundingMode.UP).toDouble()
        binding.servicefeb.text = getString(R.string.service)+" " + ((totalServiceFeb/totalCategoryFeb)*100).toBigDecimal().setScale(1, RoundingMode.UP).toDouble()
        binding.funfeb.text = getString(R.string.funny)+" " + ((totalFunFeb/totalCategoryFeb)*100).toBigDecimal().setScale(1, RoundingMode.UP).toDouble()

        binding.monthmarz.text = getString(R.string.mar)
        binding.pendingmarz.text = pendingMarz.toString()+" " + getString(R.string.pending)
        binding.rejectedmarz.text = rejectMarz.toString()+" " +getString(R.string.rejected)
        binding.incomemarz.text = "$ "+incomeMarz.toBigDecimal().setScale(1, RoundingMode.UP).toDouble().toString()+" " + getString(R.string.income)
        binding.outmarz.text = "$" + outMarz.toBigDecimal().setScale(1, RoundingMode.UP).toDouble().toString()+" " + getString(R.string.expenses)
           binding.transmarz.text = getString(R.string.transfer)+" " + ((totalTransferMarz/totalCategoryMarz)*100).toBigDecimal().setScale(1, RoundingMode.UP).toDouble()
          binding.servicemarz.text = getString(R.string.service)+" " + ((totalServiceMarz/totalCategoryMarz)*100).toBigDecimal().setScale(1, RoundingMode.UP).toDouble()
         binding.funmarz.text = getString(R.string.funny)+" " + ((totalFunMarz/totalCategoryMarz)*100).toBigDecimal().setScale(1, RoundingMode.UP).toDouble()


        binding.monthabr.text = getString(R.string.abr)
        binding.pendingabr.text = pendingApr.toString() +" "+ getString(R.string.pending)
        binding.rejectedabr.text = rejectApr.toString()+" " +getString(R.string.rejected)
        binding.incomeabr.text = "$ "+incomeApr.toBigDecimal().setScale(1, RoundingMode.UP).toDouble().toString()+" " + getString(R.string.income)
        binding.outabr.text = "$" + outApr.toBigDecimal().setScale(1, RoundingMode.UP).toDouble().toString() +" "+ getString(R.string.expenses)
           binding.transabr.text = getString(R.string.transfer)+" " +  ((totalTransferApr/totalCategoryApr)*100).toBigDecimal().setScale(1, RoundingMode.UP).toDouble()
          binding.serviceabr.text = getString(R.string.service)+" " +  ((totalServiceApr/totalCategoryApr)*100).toBigDecimal().setScale(1, RoundingMode.UP).toDouble()
         binding.funabr.text = getString(R.string.funny)+" " + ( (totalFunApr/totalCategoryApr)*100).toBigDecimal().setScale(1, RoundingMode.UP).toDouble()


        binding.monthmay.text = getString(R.string.may)
        binding.pendingmay.text = pendingMay.toString() +" "+ getString(R.string.pending)
        binding.rejectedmay.text = rejectMay.toString()+" " +getString(R.string.rejected)
        binding.incomemay.text = "$ "+incomeMay.toBigDecimal().setScale(1, RoundingMode.UP).toDouble().toString()+" " + getString(R.string.income)
        binding.outmay.text = "$" + outMay.toBigDecimal().setScale(1, RoundingMode.UP).toDouble().toString() +" "+ getString(R.string.expenses)
           binding.transmay.text = getString(R.string.transfer)+" " +  ((totalTransMay/totalCategoryMay)*100).toBigDecimal().setScale(1, RoundingMode.UP).toDouble()
          binding.servicemay.text = getString(R.string.service)+" " +  ((totalServiceMay/totalCategoryMay)*100).toBigDecimal().setScale(1, RoundingMode.UP).toDouble()
         binding.funmay.text = getString(R.string.funny)+" " +  ((totalFunMay/totalCategoryMay)*100).toBigDecimal().setScale(1, RoundingMode.UP).toDouble()


        binding.monthjun.text = getString(R.string.jun)
        binding.pendingjun.text = pendingJun.toString()+" " + getString(R.string.pending)
        binding.rejectedjun.text = rejectJun.toString()+" " +getString(R.string.rejected)
        binding.incomejun.text = "$ "+incomeJun.toBigDecimal().setScale(1, RoundingMode.UP).toDouble().toString()+" " + getString(R.string.income)
        binding.outjun.text = "$" + outJun.toBigDecimal().setScale(1, RoundingMode.UP).toDouble().toString()+" " + getString(R.string.expenses)
           binding.transjun.text = getString(R.string.transfer)+" " +  ((totalTransJun/totalCategoryJun)*100).toBigDecimal().setScale(1, RoundingMode.UP).toDouble()
          binding.servicejun.text = getString(R.string.service) +  ((totalServiceJun/totalCategoryJun)*100).toBigDecimal().setScale(1, RoundingMode.UP).toDouble()
         binding.funjun.text = getString(R.string.funny) +" "+  ((totalFunJun/totalCategoryJun)*100).toBigDecimal().setScale(1, RoundingMode.UP).toDouble()


        binding.monthjul.text = getString(R.string.jul)
        binding.pendingjul.text = pendingJul.toString()+" " + getString(R.string.pending)
        binding.rejectedjul.text = rejecJul.toString()+" " +getString(R.string.rejected)
        binding.incomejul.text = "$ "+incomeJul.toBigDecimal().setScale(1, RoundingMode.UP).toDouble().toString() +" "+ getString(R.string.income)
        binding.outjul.text = "$" + outJul.toBigDecimal().setScale(1, RoundingMode.UP).toDouble().toString()+" " + getString(R.string.expenses)
           binding.transjul.text = getString(R.string.transfer)+" " +  ((totalTransJul/totalCategoryJul)*100).toBigDecimal().setScale(1, RoundingMode.UP).toDouble()
          binding.servicejul.text = getString(R.string.service)+" " +  ((totalServiceJul/totalCategoryJul)*100).toBigDecimal().setScale(1, RoundingMode.UP).toDouble()
         binding.funjul.text = getString(R.string.funny)+" " + ( (totalFunJul/totalCategoryJul)*100).toBigDecimal().setScale(1, RoundingMode.UP).toDouble()


        binding.monthaug.text = getString(R.string.aug)
        binding.pendingaug.text = pendingAug.toString()+" " + getString(R.string.pending)
        binding.rejectedaug.text = rejectAug.toString()+" " +getString(R.string.rejected)
       binding.incomeaug.text = "$ "+incomeAug.toBigDecimal().setScale(1, RoundingMode.UP).toDouble().toString() +" "+ getString(R.string.income)
        binding.outaug.text = "$" + outAug.toBigDecimal().setScale(1, RoundingMode.UP).toDouble().toString() +" "+ getString(R.string.expenses)
           binding.transaug.text = getString(R.string.transfer) +" "+  ((totalTransAug/totalCategoryAug)*100).toBigDecimal().setScale(1, RoundingMode.UP).toDouble()
          binding.serviceaug.text = getString(R.string.service)+" " +  ((totalServiceAug/totalCategoryAug)*100).toBigDecimal().setScale(1, RoundingMode.UP).toDouble()
         binding.funaug.text = getString(R.string.funny)+" " +  ((totalFunAug/totalCategoryAug)*100).toBigDecimal().setScale(1, RoundingMode.UP).toDouble()

        binding.monthsep.text = getString(R.string.sep)
        binding.pendingsep.text = pendingSep.toString()+" " + getString(R.string.pending)
        binding.rejectedsep.text = rejectSep.toString()+" " +getString(R.string.rejected)
        binding.incomesep.text = "$ "+incomeSep.toBigDecimal().setScale(1, RoundingMode.UP).toDouble().toString()+" " + getString(R.string.income)
        binding.outsep.text = "$" + outSep.toBigDecimal().setScale(1, RoundingMode.UP).toDouble().toString() +" "+ getString(R.string.expenses)
          binding.transsep.text = getString(R.string.transfer)+" " +  ((totalTransSep/totalCategorySep)*100).toBigDecimal().setScale(1, RoundingMode.UP).toDouble()
          binding.servicesep.text = getString(R.string.service)+" " + ( (totalServiceSep/totalCategorySep)*100).toBigDecimal().setScale(1, RoundingMode.UP).toDouble()
         binding.funsep.text = getString(R.string.funny)+" " +  ((totalFunSep/totalCategorySep)*100).toBigDecimal().setScale(1, RoundingMode.UP).toDouble()

        binding.monthoct.text = getString(R.string.oct)
        binding.pendingoct.text = pendingOct.toString() +" "+ getString(R.string.pending)
        binding.rejectedoct.text = rejectOct.toString()+" " +getString(R.string.rejected)
       binding.incomeoct.text = "$ "+incomeOct.toBigDecimal().setScale(1, RoundingMode.UP).toDouble().toString()+" " + getString(R.string.income)
        binding.outoct.text = "$" + outOct.toBigDecimal().setScale(1, RoundingMode.UP).toDouble().toString() +" "+ getString(R.string.expenses)
           binding.transoct.text = getString(R.string.transfer)+" " +  ((totalTransOct/totalCategoryOct)*100).toBigDecimal().setScale(1, RoundingMode.UP).toDouble()
          binding.serviceoct.text = getString(R.string.service)+" " + ( (totalServiceOct/totalCategoryOct)*100).toBigDecimal().setScale(1, RoundingMode.UP).toDouble()
         binding.funoct.text = getString(R.string.funny)+" " +  ((totalFunOct/totalCategoryOct)*100).toBigDecimal().setScale(1, RoundingMode.UP).toDouble()

        binding.monthnov.text = getString(R.string.nov)
        binding.pendingnov.text = pendingNov.toString()+" " + getString(R.string.pending)
        binding.rejectednov.text = rejectNov.toString() +" "+getString(R.string.rejected)
        binding.incomenov.text = "$ "+incomeNov.toBigDecimal().setScale(1, RoundingMode.UP).toDouble().toString()+" " + getString(R.string.income)
        binding.outnov.text = "$" + outNov.toBigDecimal().setScale(1, RoundingMode.UP).toDouble().toString() +" "+ getString(R.string.expenses)
           binding.transnov.text = getString(R.string.transfer)+" " +  ((totalTransNov/totalCategoryNov)*100).toBigDecimal().setScale(1, RoundingMode.UP).toDouble()
          binding.servicenov.text = getString(R.string.service)+" " +  ((totalServiceNov/totalCategoryNov)*100).toBigDecimal().setScale(1, RoundingMode.UP).toDouble()
         binding.funnov.text = getString(R.string.funny)+" " +  ((totalFunNov/totalCategoryNov)*100).toBigDecimal().setScale(1, RoundingMode.UP).toDouble()

        binding.monthdec.text = getString(R.string.dec)
        binding.pendingdec.text = pendingDec.toString() +" "+ getString(R.string.pending)
        binding.rejectedndec.text = rejectDec.toString()+" " +getString(R.string.rejected)
        binding.incomedec.text = "$ "+incomeDec.toBigDecimal().setScale(1, RoundingMode.UP).toDouble().toString() +" "+ getString(R.string.income)
        binding.outdec.text = "$" + outDec.toBigDecimal().setScale(1, RoundingMode.UP).toDouble().toString()+" " + getString(R.string.expenses)
          binding.transdec.text = getString(R.string.transfer)+" " + ((totalTransDec/totalCategoryDec)*100).toBigDecimal().setScale(1, RoundingMode.UP).toDouble()
          binding.servicedec.text = getString(R.string.service)+" " +  ((totalServiceDec/totalCategoryDec)*100).toBigDecimal().setScale(1, RoundingMode.UP).toDouble()
         binding.fundec.text = getString(R.string.funny) +" "+  ((totalFunDec/totalCategoryDec)*100).toBigDecimal().setScale(1, RoundingMode.UP).toDouble()



    }

}